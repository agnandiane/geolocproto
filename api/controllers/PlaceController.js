/**
 * PlaceController
 *
 * @description :: Server-side logic for managing places
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  create: function (req, res) {
    console.log(req.body);
    Place.create(req.body).then(function (place){
      return res.status(200).send({
        code: 200,
         place: place
      });
    });
  },

  get: function(req, res){
    Place.find().then(function(place){
      return res.status(200).send({
        code: 200,
         place: place
      });
       
    })
  },

  getNear: function(req, res){
    Place.native(function(err, collection){
      collection.find({ 
        'gps' : { 
          $near : { 
            $geometry: { 
              type: 'Point', 
              coordinates: [
                parseFloat(req.param('long')),
                parseFloat(req.param('lat'))
            ] } }, 
        $maxDistance: 100 } } 
        ).toArray(function (err, result){

        if (err){
          return res.status(500).send({
            code: 500,
            message: "T'as fail grosse quiche !",
            error: err
          });
        }
        else{
          console.log(result)
          return res.status(200).send({
            code: 200,
            message: "YAY !",
            result: result
          });
        }
      }
      )
    });
  },

  nearestLocation: function (req, res){
    Place.native(function(err, collection){
      collection.aggregate([{
          $geoNear:{
            near:[
                parseFloat(req.param('long')),
                parseFloat(req.param('lat')),
              ],
            distanceField: 'distance',
            distanceMultiplier: 6378000,
            spherical: true,
            limit:5 
          }
      }],function (err, result){
        if (err){
          return res.status(500).send({
            code: 500,
            message: "T'as fail grosse quiche !",
            error: err
          });
        }
        else{
          return res.status(200).send({
            code: 200,
            message: "YAY with distance !",
            result: result
          });
        }
      });
    });
  }
};


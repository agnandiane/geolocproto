/**
* Place.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  // Enforce model schema in the case of schemaless databases
  schema: true,

  attributes: {
    uid: {type: 'string', unique: true},
    name: {type:'string'},
    address: {type:'string'},
// /!\ Should always be  longitude - latitude format/order 
    gps:{type:'json'}
  }
};

